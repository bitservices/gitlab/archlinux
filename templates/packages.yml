###############################################################################

stages:
  - test
  - gather
  - build
  - publish
  - release

###############################################################################

variables:
  REPO_NAME: bitservices

###############################################################################

before_script:
  - test -n "${PACKAGE_ARCH}"
  - test -n "${PACKAGE_NAME}"
  - test -n "${REPO_NAME}"

###############################################################################

test:
  stage: test
  image:
    name: registry.gitlab.com/bitservices/containers/makepkg:20241114
    entrypoint: [ "" ]
  rules:
    - if: $CI_COMMIT_BRANCH
  script:
    - echo "-- Verifying Archlinux Package Sources --"
    - hash runmakepkg
    - runmakepkg --syncdeps --verifysource --noconfirm

###############################################################################

gather:
  stage: gather
  image:
    name: amazon/aws-cli:2.21.0
    entrypoint: [ "" ]
  rules:
    - if: $CI_COMMIT_TAG
  variables:
    GIT_STRATEGY: none
  script:
    - echo "-- Gathering Existing Repository Index --"
    - hash aws
    - test -n "${AWS_ACCESS_KEY_ID}"
    - test -n "${AWS_SECRET_ACCESS_KEY}"
    - test -n "${AWS_S3_BUCKET}"
    - mkdir "original"
    - if [ "$(aws s3api list-objects-v2 --bucket "${AWS_S3_BUCKET}" --prefix "${REPO_NAME}/x86_64/" --query "contains(Contents[].Key || [ 'none' ], '${REPO_NAME}/x86_64/${REPO_NAME}.db')")" == "true" ]; then aws s3 cp "s3://${AWS_S3_BUCKET}/${REPO_NAME}/x86_64/${REPO_NAME}.db" "original/${REPO_NAME}.db.tar.zst"; fi
    - if [ "$(aws s3api list-objects-v2 --bucket "${AWS_S3_BUCKET}" --prefix "${REPO_NAME}/x86_64/" --query "contains(Contents[].Key || [ 'none' ], '${REPO_NAME}/x86_64/${REPO_NAME}.db.sig')")" == "true" ]; then aws s3 cp "s3://${AWS_S3_BUCKET}/${REPO_NAME}/x86_64/${REPO_NAME}.db.sig" "original/${REPO_NAME}.db.tar.zst.sig"; fi
    - if [ "$(aws s3api list-objects-v2 --bucket "${AWS_S3_BUCKET}" --prefix "${REPO_NAME}/x86_64/" --query "contains(Contents[].Key || [ 'none' ], '${REPO_NAME}/x86_64/${REPO_NAME}.files')")" == "true" ]; then aws s3 cp "s3://${AWS_S3_BUCKET}/${REPO_NAME}/x86_64/${REPO_NAME}.files" "original/${REPO_NAME}.files.tar.zst"; fi
    - if [ "$(aws s3api list-objects-v2 --bucket "${AWS_S3_BUCKET}" --prefix "${REPO_NAME}/x86_64/" --query "contains(Contents[].Key || [ 'none' ], '${REPO_NAME}/x86_64/${REPO_NAME}.files.sig')")" == "true" ]; then aws s3 cp "s3://${AWS_S3_BUCKET}/${REPO_NAME}/x86_64/${REPO_NAME}.files.sig" "original/${REPO_NAME}.files.tar.zst.sig"; fi
  artifacts:
    paths:
      - original
    expire_in: 1 day

###############################################################################

build:
  stage: build
  image:
    name: registry.gitlab.com/bitservices/containers/makepkg:20241114
    entrypoint: [ "" ]
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - echo "-- Building Archlinux Package --"
    - hash gpg
    - hash runmakepkg
    - test -n "${PACMAN_GPG_ID}"
    - test -n "${PACMAN_GPG_KEY}"
    - test -f "${PACMAN_GPG_KEY}"
    - eval $(cat PKGBUILD | grep pkgver=)
    - eval $(cat PKGBUILD | grep pkgrel=)
    - PACKAGE_VERSION="${pkgver}-${pkgrel}"
    - if [ ! "${PACKAGE_VERSION}" == "${CI_COMMIT_TAG}" ]; then exit 1; fi
    - gpg --import "${PACMAN_GPG_KEY}"
    - runmakepkg --syncdeps --sign --key "${PACMAN_GPG_ID}" --noconfirm
    - mkdir "new"
    - mv -v "${PACKAGE_NAME}-${CI_COMMIT_TAG}-${PACKAGE_ARCH}.pkg.tar.zst" "new/${PACKAGE_NAME}-${CI_COMMIT_TAG}-${PACKAGE_ARCH}.pkg.tar.zst"
    - mv -v "${PACKAGE_NAME}-${CI_COMMIT_TAG}-${PACKAGE_ARCH}.pkg.tar.zst.sig" "new/${PACKAGE_NAME}-${CI_COMMIT_TAG}-${PACKAGE_ARCH}.pkg.tar.zst.sig"
    - if [ -f "original/${REPO_NAME}.db.tar.zst" ]; then cp -v "original/${REPO_NAME}.db.tar.zst" "new/${REPO_NAME}.db.tar.zst"; fi
    - if [ -f "original/${REPO_NAME}.db.tar.zst.sig" ]; then cp -v "original/${REPO_NAME}.db.tar.zst.sig" "new/${REPO_NAME}.db.tar.zst.sig"; fi
    - if [ -f "original/${REPO_NAME}.files.tar.zst" ]; then cp -v "original/${REPO_NAME}.files.tar.zst" "new/${REPO_NAME}.files.tar.zst"; fi
    - if [ -f "original/${REPO_NAME}.files.tar.zst.sig" ]; then cp -v "original/${REPO_NAME}.files.tar.zst.sig" "new/${REPO_NAME}.files.tar.zst.sig"; fi
    - repo-add --sign --key "${PACMAN_GPG_ID}" "new/${REPO_NAME}.db.tar.zst" "new/${PACKAGE_NAME}-${CI_COMMIT_TAG}-${PACKAGE_ARCH}.pkg.tar.zst"
  artifacts:
    paths:
      - new
    expire_in: 1 day

###############################################################################

publish:
  stage: publish
  image:
    name: amazon/aws-cli:2.21.0
    entrypoint: [ "" ]
  rules:
    - if: $CI_COMMIT_TAG
  variables:
    GIT_STRATEGY: none
  script:
    - echo "-- Publishing Package and New Repository Index --"
    - hash aws
    - test -n "${AWS_ACCESS_KEY_ID}"
    - test -n "${AWS_SECRET_ACCESS_KEY}"
    - test -n "${AWS_S3_BUCKET}"
    - aws s3 cp "new/${PACKAGE_NAME}-${CI_COMMIT_TAG}-${PACKAGE_ARCH}.pkg.tar.zst" "s3://${AWS_S3_BUCKET}/${REPO_NAME}/x86_64/${PACKAGE_NAME}-${CI_COMMIT_TAG}-${PACKAGE_ARCH}.pkg.tar.zst"
    - aws s3 cp "new/${PACKAGE_NAME}-${CI_COMMIT_TAG}-${PACKAGE_ARCH}.pkg.tar.zst.sig" "s3://${AWS_S3_BUCKET}/${REPO_NAME}/x86_64/${PACKAGE_NAME}-${CI_COMMIT_TAG}-${PACKAGE_ARCH}.pkg.tar.zst.sig"
    - aws s3 cp "new/${REPO_NAME}.db.tar.zst" "s3://${AWS_S3_BUCKET}/${REPO_NAME}/x86_64/${REPO_NAME}.db"
    - aws s3 cp "new/${REPO_NAME}.db.tar.zst.sig" "s3://${AWS_S3_BUCKET}/${REPO_NAME}/x86_64/${REPO_NAME}.db.sig"
    - aws s3 cp "new/${REPO_NAME}.files.tar.zst" "s3://${AWS_S3_BUCKET}/${REPO_NAME}/x86_64/${REPO_NAME}.files"
    - aws s3 cp "new/${REPO_NAME}.files.tar.zst.sig" "s3://${AWS_S3_BUCKET}/${REPO_NAME}/x86_64/${REPO_NAME}.files.sig"
    - if [ -f "original/${REPO_NAME}.db.tar.zst" ]; then aws s3 cp "original/${REPO_NAME}.db.tar.zst" "s3://${AWS_S3_BUCKET}/${REPO_NAME}/x86_64/${REPO_NAME}.db.bak"; fi
    - if [ -f "original/${REPO_NAME}.db.tar.zst.sig" ]; then aws s3 cp "original/${REPO_NAME}.db.tar.zst.sig" "s3://${AWS_S3_BUCKET}/${REPO_NAME}/x86_64/${REPO_NAME}.db.sig.bak"; fi
    - if [ -f "original/${REPO_NAME}.files.tar.zst" ]; then aws s3 cp "original/${REPO_NAME}.files.tar.zst" "s3://${AWS_S3_BUCKET}/${REPO_NAME}/x86_64/${REPO_NAME}.files.bak"; fi
    - if [ -f "original/${REPO_NAME}.files.tar.zst.sig" ]; then aws s3 cp "original/${REPO_NAME}.files.tar.zst.sig" "s3://${AWS_S3_BUCKET}/${REPO_NAME}/x86_64/${REPO_NAME}.files.sig.bak"; fi
    - echo "-- Begin Invalidate CloudFront Cache --"
    - CLOUDFRONT_DISTRUBUTION="$(aws cloudfront list-distributions --query "DistributionList.Items[*].{id:Id,origins:Origins.Items[*].DomainName}[?contains(origins, '${AWS_S3_BUCKET}.s3.amazonaws.com')].id" --output text)"
    - test -n "${CLOUDFRONT_DISTRUBUTION}"
    - aws cloudfront create-invalidation --distribution-id "${CLOUDFRONT_DISTRUBUTION}" --paths "/${REPO_NAME}/x86_64/${REPO_NAME}.*" --query 'Invalidation.Id' --output text

###############################################################################

release:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:v0.19.0
  dependencies: []
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - echo "-- Releasing Package --"
    - hash release-cli
    - test -n "${REPO_BASE}"
    - release-cli create --name "Release ${CI_COMMIT_TAG}" --tag-name "${CI_COMMIT_TAG}" --assets-link "{\"name\":\"${PACKAGE_NAME}-${CI_COMMIT_TAG}-${PACKAGE_ARCH}.pkg.tar.zst\",\"url\":\"${REPO_BASE}/${REPO_NAME}/x86_64/${PACKAGE_NAME}-${CI_COMMIT_TAG}-${PACKAGE_ARCH}.pkg.tar.zst\"}"

###############################################################################
